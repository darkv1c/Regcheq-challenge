import { createLocalVue, mount } from '@vue/test-utils'
import { EditProduct } from 'pages/products/__components__'
import { productsMock } from 'src/axios/__mocks__/responses/products'
import { PiniaVuePlugin } from 'pinia'
import { createTestingPinia } from '@pinia/testing'
import { useProductsStore } from 'store'
import { Input, Select, Option, Upload } from 'element-ui'
import { categoriesMock } from 'src/axios/__mocks__/responses/categories'

const localVue = createLocalVue()
localVue.use(PiniaVuePlugin)

describe('Edit product modal', () => {
  let wrapper = mount(EditProduct, {
    ...localVue,
    components: {
      'el-input': Input,
      'el-select': Select,
      'el-option': Option,
      'el-upload': Upload
    },
    pinia: createTestingPinia({
      initialState: {
        products: {
          currentProduct: productsMock[0]
        },
        categories: {
          categories: categoriesMock
        }
      }
    })
  })

  it('should render itself if there is a product selected', () => {
    const productStore = useProductsStore();


    const TITLE = 'products.edit-product'
    expect(wrapper.text()).not.toContain(TITLE)

    productStore.currentProduct = productsMock[0];

    expect(wrapper.html()).toContain(TITLE)
  })
  it('should render title input', () => {
    const PLACEHOLDER = 'products.title-placeholder'
    const titleInput = wrapper.findAll('.el-input__inner').filter(
      input => input.attributes().placeholder === PLACEHOLDER
    )
    expect(titleInput).toHaveLength(1);
  })
  it('Title input must have a preloaded value', () => {
    const PLACEHOLDER = 'products.title-placeholder'

    const titleInput = wrapper.findAll('.el-input').filter(
      input => input.html().includes(PLACEHOLDER)
    ).at(0)
    expect(titleInput.props().value).toBe(productsMock[0].title);
  })
  it('should render description input', () => {
    const PLACEHOLDER = 'products.description-placeholder'
    const descriptionInput = wrapper.findAll('.el-input__inner').filter(
      input => input.attributes().placeholder === PLACEHOLDER
    )
    expect(descriptionInput).toHaveLength(1);
  })
  it('Description input must have a preloaded value', () => {
    const PLACEHOLDER = 'products.description-placeholder'

    const titleInput = wrapper.findAll('.el-input').filter(
      input => input.html().includes(PLACEHOLDER)
    ).at(0)
    expect(titleInput.props().value).toBe(productsMock[0].description);
  })
  it('should render categories dropdown', () => {
    const PLACEHOLDER = 'products.category-placeholder'
    const categoryDropdown = wrapper.findAll('.el-input__inner').filter(
      input => input.attributes().placeholder === PLACEHOLDER
    )
    expect(categoryDropdown).toHaveLength(1);
  })
  it('Categories dropdown must have a preloaded value', async () => {
    const PLACEHOLDER = 'products.category-placeholder'

    const categoryDropdown = wrapper.findAll('.el-input').filter(
      input => input.html().includes(PLACEHOLDER)
    ).at(0)
    expect(categoryDropdown.props().value).toBe(productsMock[0].category.name);
  })
  it('should select option on click and set it to current product', async () => {
    const PLACEHOLDER = 'products.category-placeholder'
    const productStore = useProductsStore();

    const categoriesDropdown =  wrapper.findAll('.el-input').filter(
      input => input.html().includes(PLACEHOLDER)
    ).at(0)

    await categoriesDropdown.trigger('click')

    const categoriesDropdownOptions = wrapper.find('.el-select-dropdown__list').findAll('.el-select-dropdown__item')
    await categoriesDropdownOptions.at(3).trigger('click');
    expect(categoriesDropdown.props().value).toBe(categoriesMock[3].name);
    expect(productStore.currentProduct?.category).toEqual(categoriesMock[3]);
  })
  it('should render price input', () => {
    const PLACEHOLDER = 'products.price-placeholder'
    const priceInput = wrapper.findAll('.el-input__inner').filter(
      input => input.attributes().placeholder === PLACEHOLDER
    )
    expect(priceInput).toHaveLength(1);
  })
  it("should render product's images", () => {
    const productsStore = useProductsStore()
    const productImages = wrapper.findAll('img').filter(
       image => productsStore.currentProduct?.images.includes(image.attributes().src)
      )
    expect(productImages.length).toBe(productsStore.currentProduct?.images.length)
  })
  it("should render product's images", () => {
    const productsStore = useProductsStore()
    const productImages = wrapper.findAll('img').filter(
       image => productsStore.currentProduct?.images.includes(image.attributes().src)
      )
    expect(productImages.length).toBe(productsStore.currentProduct?.images.length)
  })
  it('should render image upload option', () => {
    const uploadButton = wrapper.findComponent(Upload)
    expect(uploadButton.exists()).toBe(true);
  })
  it('should add upload images to currentProduct', async () => {
    const file = {
        name: 'image.png',
        url: 'fakeUrl'
      }
    const uploadButton = wrapper.findComponent(Upload)
    const productsStore = useProductsStore();

    uploadButton.props().onSuccess(undefined, file, productsStore.currentProduct?.images)

    expect(productsStore.currentProduct?.images).toContain(file.url)
  })
  it('should have a close button to delete images', () => {
    const closeButton = wrapper.find('[role="image eraser"]')
    expect(closeButton.exists()).toBe(true)
  })
  it('should remove image on close button click', async () => {
    const productsStore = useProductsStore()
    const image = productsStore.currentProduct?.images[1]
    expect(productsStore.currentProduct?.images).toContain(image)
    const closeButton = wrapper.findAll('[role="image eraser"]').at(1)
    closeButton.trigger('click')
    expect(productsStore.currentProduct?.images).not.toContain(image)
  })
  it('should render a save changes button', () => {
    const saveButton = wrapper.find('[role="save button"]')
    expect(saveButton.exists()).toBe(true)
  })
  it('should call api to update product on save button click', async () => {
    const { productsStore } = await updateProduct()
    expect(productsStore.updateProduct).toHaveBeenCalledWith(productsStore.currentProduct)
  })
  it('should render a delete product button', () => {
    const deleteButton = wrapper.find('[role="delete button"]')
    expect(deleteButton.exists()).toBe(true)
  })

  async function updateProduct() {
    const productsStore = useProductsStore()
    const saveButton = wrapper.find('[role="save button"]')
    await saveButton.trigger('click')
    return { productsStore }
  }
})


