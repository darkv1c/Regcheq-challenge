import { createLocalVue, mount } from '@vue/test-utils'
//@ts-ignore
import Products from 'pages/products'
import { productsMock } from 'src/axios/__mocks__/responses/products'
import { PiniaVuePlugin } from 'pinia'
import { createTestingPinia } from '@pinia/testing'
import { useCategoriesStore, useProductsStore } from 'store'

const localVue = createLocalVue()
localVue.use(PiniaVuePlugin)

describe('Products page', () => {

  let wrapper = mount(Products, {
    ...localVue,
    pinia: createTestingPinia({
      initialState: {
        products: {
          products: productsMock
        }
      }
    })
  })

  it('should get products from api', () => {

    const productsStore = useProductsStore();

    expect(productsStore.getProducts).toBeCalledTimes(1)

  })
  it('should render products info', () => {
    productsMock.map((product) => {
      expect(wrapper.text()).toContain(product.title)
      expect(wrapper.text()).toContain(product.description)
      expect(wrapper.text()).toContain(product.category.name)
      expect(wrapper.text()).toContain(product.price + '')
    })
  })
  it('should render the first image of the product', () => {
    const images = wrapper.findAll('img').filter(
      img => img.attributes().alt === 'products.image-alt'
      ).wrappers

    for (const index in images) {
      expect(images[index].attributes().src).toBe(productsMock[index].images[0])
    }
  })
  it('should render an edit icon for every product', () => {
    expect(wrapper.findAll('.el-icon-edit')).toHaveLength(productsMock.length)
  })
  it('should set currentProduct on edit icon click', async () => {
    const productsStore = useProductsStore();
    const editButton = wrapper.find('el-button');

    await editButton.trigger('click')
    expect(productsStore.currentProduct).toEqual(productsMock[0])
  })
  it('should call categories api once', () => {
    const categorisStore = useCategoriesStore()
    expect(categorisStore.getCategories).toHaveBeenCalledTimes(1)
  })
})
