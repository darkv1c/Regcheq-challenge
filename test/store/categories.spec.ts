// stores/counter.spec.ts
import { setActivePinia, createPinia } from 'pinia'
import { useCategoriesStore } from 'store'
import { fail, ERROR_MESSAGE } from 'src/axios/__mocks__/responses/products'
import { categoriesMock } from 'src/axios/__mocks__/responses/categories'

describe('Categories Store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it('gets all categories if getCategories is called with no parameters', async () => {
    const categoriesStore = useCategoriesStore();
    await categoriesStore.getCategories();
    expect(categoriesStore.categories).toEqual(categoriesMock);
  })
  // it('should throw error if getProducts fails', async () => {
  //   const productsStore = useCategoriesStore();
  //   await expect(productsStore.getCategories(fail)).rejects.toThrow(ERROR_MESSAGE);
  // })
})
