// stores/counter.spec.ts
import { setActivePinia, createPinia } from 'pinia'
import { useProductsStore } from 'store'
import { productsMock, fail, ERROR_MESSAGE } from 'src/axios/__mocks__/responses/products'

describe('Products Store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it('gets all products if getProducts is called with no parameters', async () => {
    const productsStore = useProductsStore();
    await productsStore.getProducts();
    expect(productsStore.products).toEqual(productsMock);
  })
  it('gets filtered products if getProducts is called with query params', async () => {
    const TITLE = 'test 2'
    const productsStore = useProductsStore();
    await productsStore.getProducts({
      title: TITLE
    });
    expect(productsStore.products).toEqual(productsMock.filter(prod => prod.title === TITLE));
  })
  it('should throw error if getProducts fails', async () => {
    const productsStore = useProductsStore();
    await expect(productsStore.getProducts(fail)).rejects.toThrow(ERROR_MESSAGE);
  })
})
