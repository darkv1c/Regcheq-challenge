import { Product } from "store/types/products"

export interface Data {
  selectedCategory: string | null,
  imagesList: ImageFile[],
}

export interface ImageFile {
  name: string,
  url: string
}
