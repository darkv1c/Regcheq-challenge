import { AxiosRequestConfig } from "axios"
import { categoriesMock } from "./categories"

export const productsMock = [
  {
    "id": 1,
    "title": "test 1",
    "price": 100,
    "description": "Andy shoes are designed to keeping in...",
    "category": categoriesMock[0],
    "images": [
      "product1-image1",
      "product1-image2",
      "product1-image3",
    ]
  },
  {
    "id": 2,
    "title": "test 2",
    "price": 200,
    "description": "Andy shoes are designed to keeping in...",
    "category": categoriesMock[2],
    "images": []
  },
  {
    "id": 3,
    "title": "test 3",
    "price": 300,
    "description": "Andy shoes are designed to keeping in...",
    "category": categoriesMock[1],
    "images": [
      "product3-image1",
      "product3-image2",
      "product3-image3",
      "product3-image4",
      "product3-image5",
    ]
  },
  {
    "id": 4,
    "title": "test 4",
    "price": 400,
    "description": "Andy shoes are designed to keeping in...",
    "category": categoriesMock[3],
    "images": ['product4-image1']
  }
]

/** send this as parameter to fail request */
export const fail = {
  title:  'throw error'
}

export const ERROR_MESSAGE = 'get products error'

export function getProducts(config: AxiosRequestConfig) {
  if (JSON.stringify(fail) === JSON.stringify(config.params)) {
    throw new TypeError(ERROR_MESSAGE)
  }
  if (config.params) return productsMock.filter(
    product => product.title === config.params.title
  )
  return productsMock
}
