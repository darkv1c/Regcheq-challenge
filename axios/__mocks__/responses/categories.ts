import { AxiosRequestConfig } from "axios"

export const categoriesMock = [
  {
    id: 1,
    name: 'category 1',
    image: 'image 1'
  },
  {
    id: 2,
    name: 'category 2',
    image: 'image 2'
  },
  {
    id: 3,
    name: 'category 3',
    image: 'image 3'
  },
  {
    id: 4,
    name: 'category 4',
    image: 'image 4'
  },
]

/** send this as parameter to fail request */
export const fail = {
  title:  'throw error'
}

export const ERROR_MESSAGE = 'get products error'

export function getCategories(config: AxiosRequestConfig) {
  return categoriesMock
}
