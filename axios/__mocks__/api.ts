import { AxiosRequestConfig } from 'axios'
import { getCategories } from './responses/categories'
import { getProducts } from './responses/products'

const sendRequestTo = {
  'v1/products': getProducts,
  'v1/categories': getCategories
}

export default {
  get(url: keyof typeof sendRequestTo, config: AxiosRequestConfig) {
    return {
      data: sendRequestTo[url](config)
    }
  }
}
