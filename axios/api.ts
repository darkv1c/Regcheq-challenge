import axios from 'axios'

axios.defaults.baseURL = 'https://api.escuelajs.co/api';

export default axios;
