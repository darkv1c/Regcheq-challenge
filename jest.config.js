module.exports = {
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/$1",
    "^~/(.*)$": "<rootDir>/$1",
    "^vue$": "vue/dist/vue.common.js",
    "^pages(.*)$": "<rootDir>/pages/$1",
    "^store(.*)$": "<rootDir>/store/$1",
    "^utils(.*)$": "<rootDir>/utils/$1",
    "^src(.*)$": "<rootDir>/$1",
  },
  moduleFileExtensions: ["ts", "js", "vue", "json"],
  testEnvironment: "jsdom",
  moduleFileExtensions: ["js", "jsx", "mjs", "ts", "vue"],
  transform: {
    "^.+\\.ts$": "ts-jest",
    "^.+\\.js$": "babel-jest",
    ".*\\.(vue)$": "vue-jest",
  },
  collectCoverage: true,
  collectCoverageFrom: [
    "<rootDir>/components/**/*.vue",
    "<rootDir>/pages/**/*.vue",
  ],
  testEnvironment: "jsdom",
  setupFiles: ["<rootDir>/jest.init.js"],
};
