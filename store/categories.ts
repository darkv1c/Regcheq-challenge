import { defineStore } from "pinia";
import api from "api";
import { CategoriesStore } from "./types/categories";

const initialState:CategoriesStore = {
  categories: null,
}

const useCategoriesStore = defineStore('categories', {
  state: () => initialState,
  actions: {
    async getCategories() {
      const response = await api.get('v1/categories')
      this.categories = response.data
    }
  }
})

export default useCategoriesStore
