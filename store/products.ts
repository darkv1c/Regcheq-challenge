import { defineStore } from 'pinia'
import { Product, ProductsState, QueryParams } from './types/products'
import api from 'api'

const initialState:ProductsState = {
  products: [],
  currentProduct: null
}

const useProductsStore = defineStore('products', {
  state: () => initialState,
  actions: {
    async getProducts(params?: QueryParams) {
      const response = await api.get('v1/products', { params })
      this.products = response.data
    },
    async updateProduct(params: Product) {
      // const response = await api.put(`v1/products/${params.id}`, { params })
      // this.products = response.data
    },
    async deleteProduct(id: number) {
      await api.delete(`v1/products/${id}`)
    }
  }
});

export default useProductsStore;
