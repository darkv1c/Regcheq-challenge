export interface Category {
  "id": number,
  "name": string,
  "image": string
}

export interface CategoriesStore {
  categories: Category[] | null,
}
