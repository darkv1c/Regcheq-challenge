import { Category } from "./categories"

export interface ProductsState {
  products: Product[],
  currentProduct: Product | null
}

export interface Product  {
  "id": number,
  "title": string,
  "price": number,
  "description": string,
  "category": Category,
  "images": string[]
}

export interface QueryParams {
  title?: string,
  price?: number,
  price_min?: number,
  price_max?: number,
  categoryId?: number
}
