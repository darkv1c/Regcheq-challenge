import messages from "./i18n";
import { resolve } from "path";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Regcheq-challenge",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["element-ui/lib/theme-chalk/index.css", "@/css/index.scss"],

  styleResources: {
    scss: ["@/css/*.scss"],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ["@/plugins/element-ui"],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/i18n",
    "@nuxtjs/composition-api/module",
    "@pinia/nuxt",
    "@nuxtjs/style-resources",
  ],

  i18n: {
    locales: ["es-CO"],
    defaultLocale: "es-CO",
    vueI18n: {
      fallbackLocale: "en",
      messages,
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, { isDev, isClient }) {
      config.module.rules.push({
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto",
      });
      config.resolve.alias = {
        ...config.resolve.alias,
        src: resolve(__dirname, "./"),
        api: resolve(__dirname, "axios/api"),
        store: resolve(__dirname, "store"),
        pages: resolve(__dirname, "pages"),
        utils: resolve(__dirname, "utils"),
      };
    },
    transpile: [/^element-ui/],
  },

  pwa: {
    manifest: {
      lang: "en",
      name: "Regcheq challenge",
      shortName: "RC",
      icons: [
        { src: "static/favicon.ico", sizes: "192x192", type: "image/ico" },
        { src: "static/favicon.ico", sizes: "256x256", type: "image/ico" },
        { src: "static/favicon.ico", sizes: "512x512", type: "image/ico" },
      ],
    },
  },
};
