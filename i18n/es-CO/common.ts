export default {
  'common.ok': 'Listo',
  'common.save': 'Guardar',
  'common.delete': 'Borrar',
  'common.confirm': 'Confirmar',
  'common.cancel': 'Cancelar'
}
