import common from "./common";
import pages from "./pages";

export default {
  ...pages,
  ...common
}
