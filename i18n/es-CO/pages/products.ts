export default {
  'products.image-alt': 'Imagen del producto',
  'products.title': 'Título',
  'products.description': 'Descripción',
  'products.price': 'Precio',
  'products.category': 'Categoría',
  'products.edit-product': 'Editar producto',
  'products.title-placeholder': 'Titulo',
  'products.description-placeholder': 'Descripción',
  'products.category-placeholder': 'Categoría',
  'products.price-placeholder': 'Precio',
  'products.edition-successful-message': 'Producto editado con éxito',
  'product.sure-to-delete': '¿Estás seguro de eliminar este producto?'
}
